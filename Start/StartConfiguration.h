#ifndef START_CONFIGURATION_H
#define START_CONFIGURATION_H

#include <MisConfig.h>

// Exporting of App classes
#define START_SHOW_SKETCHER
#define START_USE_DRAFTING

#endif