#ifndef START_PRECOMPILED_H
#define START_PRECOMPILED_H

#include <MisConfig.h>

// Exporting of App classes
# define AppStartExport __declspec(dllexport)
# define PartExport  __declspec(dllimport)
# define MeshExport     __declspec(dllimport)

#ifdef _PreComp_
// standard
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <assert.h>
#include <string>
#include <map>
#include <vector>
#include <set>
#include <bitset>
#include <Python.h>
#endif // _PreComp_

#endif // START_PRECOMPILED_H