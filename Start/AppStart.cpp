#include "PreCompiled.h"

#ifndef _PreComp_
# include <Python.h>
#endif

#include <MisBase/Console.h>
#include <MisBase/Interpreter.h>
#include <CXX/Extensions.hxx>
#include <CXX/Objects.hxx>

#include "StartConfiguration.h"

namespace Start {
class Module : public Py::ExtensionModule<Module>
{
public:
    Module() : Py::ExtensionModule<Module>("Start")
    {
        initialize("This module is the Start module."); // register with Python
    }

    virtual ~Module() {}
};
} // namespace Start

/* Python entry */
PyMODINIT_FUNC initStart()
{
    new Start::Module();
    Base::Console().Log("Loading Start module... done\n");
}