#include <MisConfig.h>

#ifdef _PreComp_
# undef _PreComp_
#endif

#include <stdio.h>
#include <sstream>

// MisCAD Base header
#include <MisBase/Exception.h>
#include <MisApp/Application.h>

# include <windows.h>

/** DllMain is called when DLL is loaded
 */
BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    switch (ul_reason_for_call) {
    case DLL_PROCESS_ATTACH: {
        // This name is preliminary, we pass it to Application::init() in initMisCAD()
        // which does the rest.
        char  szFileName [MAX_PATH];
        GetModuleFileName((HMODULE)hModule, szFileName, MAX_PATH-1);
        App::Application::Config()["AppHomePath"] = szFileName;
    }
    break;
    default:
        break;
    }

    return true;
}

#	define MainExport __declspec(dllexport)

extern "C"
{
    void MainExport initMisCAD() {

        // Init phase ===========================================================
        App::Application::Config()["ExeName"] = "MisCAD";
        App::Application::Config()["ExeVendor"] = "MisCAD";
        App::Application::Config()["AppDataSkipVendor"] = "true";

        int    argc=1;
        char** argv;
        argv = (char**)malloc(sizeof(char*)* (argc+1));

        argv[0] = (char*)malloc(MAX_PATH);
        strncpy(argv[0],App::Application::Config()["AppHomePath"].c_str(),MAX_PATH);
        argv[argc] = 0;

        try {
            // Inits the Application
            App::Application::init(argc,argv);
        }
        catch (const Base::Exception& e) {
            std::string appName = App::Application::Config()["ExeName"];
            std::stringstream msg;
            msg << "While initializing " << appName << " the  following exception occurred: '"
                << e.what() << "'\n\n";
            msg << "\nPlease contact the application's support team for more information.\n\n";
            printf("Initialization of %s failed:\n%s", appName.c_str(), msg.str().c_str());
        }

        free(argv[0]);
        free(argv);

        return;
    } //InitMisCAD....
} // extern "C"

