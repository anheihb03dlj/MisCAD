/***************************************************************************
 *   Copyright (c) 2004 Werner Mayer <wmayer[at]users.sourceforge.net>     *
 *                                                                         *
 *   This file is part of the FreeCAD CAx development system.              *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/


#include "PreCompiled.h"
#ifndef _PreComp_
# include <QDialogButtonBox>
# include <QDir>
# include <QFileDialog>
# include <QFileInfo>
# include <QHeaderView>
# include <QImageReader>
# include <QKeySequence>
# include <QLineEdit>
# include <QMessageBox>
# include <QTextStream>
# include <QVBoxLayout>
#endif

#include "DlgActionsImp.h"
#include "Action.h"
#include "Application.h"
#include "Command.h"
#include "BitmapFactory.h"
#include "Widgets.h"
#include "ui_DlgChooseIcon.h"

using namespace Gui::Dialog;

IconDialog::IconDialog(QWidget* parent)
  : QDialog(parent), ui(new Ui_DlgChooseIcon)
{
    ui->setupUi(this);
    ui->listWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    // signals and slots connections
    connect(ui->listWidget, SIGNAL(itemClicked (QListWidgetItem *)),
            this, SLOT(accept()));
    connect(ui->addButton, SIGNAL(clicked()),
            this, SLOT(onAddIconPath()));

    QListWidgetItem* item;
    QStringList names = BitmapFactory().findIconFiles();
    for (QStringList::Iterator it = names.begin(); it != names.end(); ++it) {
        item = new QListWidgetItem(ui->listWidget);
        //item->setIcon(QIcon(*it));
        item->setIcon(QIcon(BitmapFactory().pixmap((const char*)it->toUtf8())));
        item->setText(QFileInfo(*it).baseName());
        item->setToolTip(*it);
    }
}

IconDialog::~IconDialog()
{
    delete ui;
}

QListWidgetItem* IconDialog::currentItem() const
{
    return ui->listWidget->currentItem();
}

void IconDialog::resizeEvent(QResizeEvent*)
{
    ui->listWidget->setFlow(QListView::LeftToRight);
}

void IconDialog::onAddIconPath()
{
    // Add the user defined paths
    Base::Reference<ParameterGrp> group = App::GetApplication().GetParameterGroupByPath
        ("User parameter:BaseApp/Preferences/Bitmaps");
    std::vector<std::string> paths = group->GetASCIIs("CustomPath");
    QStringList pathList;
    for (std::vector<std::string>::iterator it = paths.begin(); it != paths.end(); ++it)
        pathList << QString::fromUtf8(it->c_str());

    IconFolders dlg(pathList, this);
    dlg.setWindowTitle(tr("Icon folders"));
    if (dlg.exec()) {
        QStringList paths = dlg.getPaths();

        // Write to user config
        group->Clear();
        int index=0;
        for (QStringList::iterator it = paths.begin(); it != paths.end(); ++it, ++index) {
            std::stringstream str;
            str << "CustomPath" << index;
            group->SetASCII(str.str().c_str(), (const char*)it->toUtf8());
        }

        QStringList search = BitmapFactory().getPaths();
        for (QStringList::iterator it = search.begin(); it != search.end(); ++it) {
            *it = QDir::toNativeSeparators(*it);
        }
        for (QStringList::iterator it = paths.begin(); it != paths.end(); ++it) {
            if (search.indexOf(*it) < 0) {
                QStringList filters;
                QList<QByteArray> formats = QImageReader::supportedImageFormats();
                for (QList<QByteArray>::iterator jt = formats.begin(); jt != formats.end(); ++jt)
                    filters << QString::fromLatin1("*.%1").arg(QString::fromLatin1(*jt).toLower());
                QDir d(*it);
                d.setNameFilters(filters);
                QFileInfoList fi = d.entryInfoList();
                for (QFileInfoList::iterator jt = fi.begin(); jt != fi.end(); ++jt) {
                    QListWidgetItem* item;
                    QString file = jt->absoluteFilePath();
                    item = new QListWidgetItem(ui->listWidget);
                    item->setIcon(QIcon(file));
                    item->setText(jt->baseName());
                    item->setToolTip(file);
                }

                BitmapFactory().addPath(*it);
            }
        }
    }
}

void DlgCustomActionsImp::on_buttonChoosePixmap_clicked()
{
    // create a dialog showing all pixmaps
    Gui::Dialog::IconDialog dlg(this);
    dlg.setModal(true);
    dlg.exec();

    pixmapLabel->clear();
    m_sPixmap = QString::null;
    if (dlg.result() == QDialog::Accepted) {
        QListWidgetItem* item = dlg.currentItem();
        if (item) {
            m_sPixmap = item->text();
            pixmapLabel->setPixmap(item->icon().pixmap(QSize(32,32)));
        }
    }
}

QString DlgCustomActionsImp::newActionName()
{
    int id = 0;
    QString sName;
    bool bUsed;

    CommandManager& rclMan = Application::Instance->commandManager();
    std::vector<Command*> aclCurMacros = rclMan.getGroupCommands("Macros");

    do
    {
        bUsed = false;
        sName = QString::fromLatin1("Std_Macro_%1").arg( id++ );

        std::vector<Command*>::iterator it;
        for ( it = aclCurMacros.begin(); it!= aclCurMacros.end(); ++it )
        {
            if (sName == QLatin1String((*it)->getName()))
            {
                bUsed = true;
                break;
            }
        }
    } while ( bUsed );

    return sName;
}

void DlgCustomActionsImp::changeEvent(QEvent *e)
{
    if (e->type() == QEvent::LanguageChange) {
        this->retranslateUi(this);
        actionListWidget->clear();
        showActions();
        actionAccel->setText(qApp->translate("Gui::AccelLineEdit", "none"));
    }
    QWidget::changeEvent(e);
}

IconFolders::IconFolders(const QStringList& paths, QWidget* parent)
  : QDialog(parent), restart(false), maxLines(10)
{
    resize(600,400);
    QDialogButtonBox* buttonBox = new QDialogButtonBox(this);
    buttonBox->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(accepted()),
            this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()),
            this, SLOT(reject()));

    gridLayout = new QGridLayout();
    QGridLayout* mainLayout = new QGridLayout(this);
    mainLayout->addLayout(gridLayout, 0, 0, 1, 1);

    QSpacerItem* verticalSpacer = new QSpacerItem(20, 108, QSizePolicy::Minimum, QSizePolicy::Expanding);
    mainLayout->addItem(verticalSpacer, 1, 0, 1, 1);
    mainLayout->addWidget(buttonBox, 2, 0, 1, 1);

    // Add the user defined paths
    int numPaths = static_cast<int>(paths.size());
    int maxRow = this->maxLines;
    for (int row=0; row<maxRow; row++) {
        QLineEdit* edit = new QLineEdit(this);
        edit->setReadOnly(true);
        gridLayout->addWidget(edit, row, 0, 1, 1);
        QPushButton* removeButton = new QPushButton(this);
        removeButton->setIcon(BitmapFactory().iconFromTheme("list-remove"));
        gridLayout->addWidget(removeButton, row, 1, 1, 1);

        if (row < numPaths) {
            edit->setText(paths[row]);
        }
        else {
            edit->hide();
            removeButton->hide();
        }

        buttonMap.append(qMakePair<QLineEdit*, QPushButton*>(edit, removeButton));
        connect(removeButton, SIGNAL(clicked()), this, SLOT(removeFolder()));
    }

    textLabel = new QLabel(this);
    textLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    textLabel->setText(tr("Add or remove custom icon folders"));
    addButton = new QPushButton(this);
    addButton->setIcon(BitmapFactory().iconFromTheme("list-add"));
    gridLayout->addWidget(textLabel, maxRow, 0, 1, 1);
    gridLayout->addWidget(addButton, maxRow, 1, 1, 1);

    connect(addButton, SIGNAL(clicked()), this, SLOT(addFolder()));
    if (numPaths >= this->maxLines)
        addButton->setDisabled(true);
}

IconFolders::~IconFolders()
{
}

void IconFolders::addFolder()
{
    int countHidden = -1;
    QStringList paths;
    for (QList< QPair<QLineEdit*, QPushButton*> >::iterator it = buttonMap.begin(); it != buttonMap.end(); ++it) {
        if (it->first->isHidden()) {
            countHidden++;
            if (countHidden == 0) {
                QString dir = QFileDialog::getExistingDirectory(this, IconDialog::tr("Add icon folder"), QString());
                if (!dir.isEmpty() && paths.indexOf(dir) < 0) {
                    QLineEdit* edit = it->first;
                    edit->setVisible(true);
                    edit->setText(dir);
                    QPushButton* removeButton = it->second;
                    removeButton->setVisible(true);
                }
            }
        }
        else {
            paths << QDir::toNativeSeparators(it->first->text());
        }
    }

    if (countHidden <= 0) {
        addButton->setDisabled(true);
    }
}

void IconFolders::removeFolder()
{
    if (!restart) {
        restart = true;
        QMessageBox::information(this, tr("Remove folder"),
            tr("Removing a folder only takes effect after an application restart."));
    }

    addButton->setEnabled(true);
    QPushButton* remove = static_cast<QPushButton*>(sender());
    QLineEdit* edit = 0;
    for (QList< QPair<QLineEdit*, QPushButton*> >::iterator it = buttonMap.begin(); it != buttonMap.end(); ++it) {
        if (it->second == remove) {
            edit = it->first;
        }
        else if (edit) {
            // move up the text of the line edits
            edit->setText(it->first->text());

            if (it->first->isVisible()) {
                edit = it->first;
                remove = it->second;
            }
            else {
                edit->hide();
                remove->hide();
                break;
            }
        }
    }
}

QStringList IconFolders::getPaths() const
{
    QStringList paths;
    for (QList< QPair<QLineEdit*, QPushButton*> >::const_iterator it = buttonMap.begin(); it != buttonMap.end(); ++it) {
        if (!it->first->isHidden()) {
            paths << QDir::toNativeSeparators(it->first->text());
        }
        else {
            break;
        }
    }
    return paths;
}

#include "moc_DlgActionsImp.cpp" 
