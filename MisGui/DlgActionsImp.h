/***************************************************************************
 *   Copyright (c) 2004 Werner Mayer <wmayer[at]users.sourceforge.net>     *
 *                                                                         *
 *   This file is part of the FreeCAD CAx development system.              *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/


#ifndef GUI_DIALOG_DLGACTIONS_IMP_H
#define GUI_DIALOG_DLGACTIONS_IMP_H

#include "PropertyPage.h"
#include <QDialog>
#include <QList>
#include <QPair>


namespace Gui {
namespace Dialog {

class Ui_DlgChooseIcon;
class IconDialog : public QDialog
{
    Q_OBJECT

public:
    IconDialog(QWidget* parent);
    ~IconDialog();
    void resizeEvent(QResizeEvent*);
    QListWidgetItem* currentItem() const;

private Q_SLOTS:
    void onAddIconPath();

private:
    Ui_DlgChooseIcon *ui;
};

class IconFolders : public QDialog
{
    Q_OBJECT

public:
    IconFolders(const QStringList&, QWidget* parent);
    ~IconFolders();
    QStringList getPaths() const;

private Q_SLOTS:
    void addFolder();
    void removeFolder();

private:
    bool restart;
    int maxLines;
    QGridLayout* gridLayout;
    QLabel* textLabel;
    QPushButton* addButton;
    QList< QPair<QLineEdit*, QPushButton*> > buttonMap;
};

} // namespace Dialog
} // namespace Gui

#endif // GUI_DIALOG_DLGACTIONS_IMP_H
