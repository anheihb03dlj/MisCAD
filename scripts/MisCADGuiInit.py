# imports the one and only
import MisCAD, MisCADGui

# shortcuts
Gui = MisCADGui

# Important definitions
class Workbench:
    """The workbench base class."""
    MenuText = ""
    ToolTip = ""
    
    def Initialize(self):
        """Initializes this workbench."""
        App.PrintWarning(str(self) + ": Workbench.Initialize() not implemented in subclass!")
    def ContextMenu(self, recipient):
        pass
    def appendToolbar(self,name,cmds):
        self.__Workbench__.appendToolbar(name, cmds)
    def removeToolbar(self,name):
        self.__Workbench__.removeToolbar(name)
    def appendCommandbar(self,name,cmds):
        self.__Workbench__.appendCommandbar(name, cmds)
    def removeCommandbar(self,name):
        self.__Workbench__.removeCommandbar(name)
    def appendMenu(self,name,cmds):
        self.__Workbench__.appendMenu(name, cmds)
    def removeMenu(self,name):
        self.__Workbench__.removeMenu(name)
    def listMenus(self):
        return self.__Workbench__.listMenus()
    def appendContextMenu(self,name,cmds):
        self.__Workbench__.appendContextMenu(name, cmds)
    def removeContextMenu(self,name):
        self.__Workbench__.removeContextMenu(name)
    def name(self):
        return self.__Workbench__.name()
    def GetClassName(self):
        """Return the name of the associated C++ class."""
        # as default use this to simplify writing workbenches in Python 
        return "Gui::PythonWorkbench"


class StandardWorkbench ( Workbench ):
    """A workbench defines the tool bars, command bars, menus, 
context menu and dockable windows of the main window.
    """
    def Initialize(self):
        """Initialize this workbench."""
        # load the module
        Log ('Init: Loading MisCAD GUI\n')
    def GetClassName(self):
        """Return the name of the associated C++ class."""
        return "Gui::StdWorkbench"

def InitApplications():
    import sys,os,traceback,cStringIO
    # Searching modules dirs +++++++++++++++++++++++++++++++++++++++++++++++++++
    # (additional module paths are already cached)
    ModDirs = MisCAD.__path__
    #print ModDirs
    Log('Init:   Searching modules...\n')
    for Dir in ModDirs:
        if ((Dir != '') & (Dir != 'CVS') & (Dir != '__init__.py')):
            InstallFile = os.path.join(Dir,"InitGui.py")
            if (os.path.exists(InstallFile)):
                try:
                    #execfile(InstallFile)
                    exec open(InstallFile).read()
                except Exception, inst:
                    Log('Init:      Initializing ' + Dir + '... failed\n')
                    Log('-'*100+'\n')
                    output=cStringIO.StringIO()
                    traceback.print_exc(file=output)
                    Log(output.getvalue())
                    Log('-'*100+'\n')
                    Err('During initialization the error ' + str(inst).decode('ascii','replace') + ' occurred in ' + InstallFile + '\n')
                else:
                    Log('Init:      Initializing ' + Dir + '... done\n')
            else:
                Log('Init:      Initializing ' + Dir + '(InitGui.py not found)... ignore\n')


Log ('Init: Running MisCADGuiInit.py start script...\n')

# init the gui

# signal that the gui is up
App.GuiUp = 1
App.Gui = MisCADGui
MisCADGui.Workbench = Workbench

Gui.addWorkbench(StandardWorkbench())

# init modules
InitApplications()

# set standard workbench (needed as fallback)
Gui.activateWorkbench("StandardWorkbench")

# Register .py, .FCScript and .FCMacro
MisCAD.addImportType("Inventor V2.1 (*.iv)","MisCADGui")
MisCAD.addImportType("VRML V2.0 (*.wrl *.vrml *.wrz *.wrl.gz)","MisCADGui")
MisCAD.addImportType("Python (*.py *.FCMacro *.FCScript)","MisCADGui")
MisCAD.addExportType("Inventor V2.1 (*.iv)","MisCADGui")
MisCAD.addExportType("VRML V2.0 (*.wrl *.vrml *.wrz *.wrl.gz)","MisCADGui")
#MisCAD.addExportType("IDTF (for 3D PDF) (*.idtf)","MisCADGui")
#MisCAD.addExportType("3D View (*.svg)","MisCADGui")
MisCAD.addExportType("Portable Document Format (*.pdf)","MisCADGui")

del(InitApplications)
del(StandardWorkbench)


Log ('Init: Running MisCADGuiInit.py start script... done\n')