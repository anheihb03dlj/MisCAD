#ifndef STARTGUI_PRECOMPILED_H
#define STARTGUI_PRECOMPILED_H

#include <MisConfig.h>

// Importing of App classes
# define StartAppExport __declspec(dllimport)
# define StartGuiExport __declspec(dllexport)

#ifdef _PreComp_

// Python
#include <Python.h>

// standard
#include <iostream>
#include <assert.h>
#include <cmath>

// STL
#include <vector>
#include <map>
#include <string>
#include <list>
#include <set>
#include <algorithm>
#include <stack>
#include <queue>
#include <bitset>

#ifdef FC_OS_WIN32
# include <windows.h>
#endif

// Qt Toolkit
#ifndef __Qt4All__
# include <MisGui/Qt4All.h>
#endif

#endif //_PreComp_

#endif // STARTGUI_PRECOMPILED_H
