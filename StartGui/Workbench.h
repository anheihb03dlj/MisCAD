#ifndef STARTGUI_WORKBENCH_H
#define STARTGUI_WORKBENCH_H

#include <MisGui/Workbench.h>

namespace StartGui {

/**
 * @author Werner Mayer
 */
class StartGuiExport Workbench : public Gui::StdWorkbench
{
    TYPESYSTEM_HEADER();

public:
  Workbench();
  virtual ~Workbench();

    /** Defines the standard context menu. */
    virtual void setupContextMenu(const char* recipient,Gui::MenuItem*) const;
    /** Run some actions when the workbench gets activated. */
    virtual void activated();

protected:
    /** Defines the standard menus. */
    virtual Gui::MenuItem* setupMenuBar() const;
    /** Defines the standard toolbars. */
    virtual Gui::ToolBarItem* setupToolBars() const;
    /** Defines the standard command bars. */
    virtual Gui::ToolBarItem* setupCommandBars() const;
    /** Returns a DockWindowItems structure of dock windows this workbench. */
    virtual Gui::DockWindowItems* setupDockWindows() const;

}; // namespace StartGui

}
#endif // START_WORKBENCH_H 
