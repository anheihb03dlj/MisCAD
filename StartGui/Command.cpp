#include "PreCompiled.h"
#ifndef _PreComp_
#endif

#include <MisGui/Application.h>
#include <MisGui/Command.h>
#include <MisGui/MainWindow.h>
#include <MisGui/FileDialog.h>

using namespace std;

DEF_STD_CMD(CmdStartConstraintAxle);

CmdStartConstraintAxle::CmdStartConstraintAxle()
	:Command("Start_ConstraintAxle")
{
    sAppModule      = "Start";
    sGroup          = QT_TR_NOOP("Start");
    sMenuText       = QT_TR_NOOP("Constraint Axle...");
    sToolTipText    = QT_TR_NOOP("set a axle constraint between two objects");
    sWhatsThis      = sToolTipText;
    sStatusTip      = sToolTipText;
    sPixmap         = "actions/document-new";
}


void CmdStartConstraintAxle::activated(int iMsg)
{
    // load the file with the module
    //Command::doCommand(Command::Gui, "import Start, StartGui");
      
}



void CreateStartCommands(void)
{
    Gui::CommandManager &rcCmdMgr = Gui::Application::Instance->commandManager();

    rcCmdMgr.addCommand(new CmdStartConstraintAxle());
 }
