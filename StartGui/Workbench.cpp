#include "PreCompiled.h"

#ifndef _PreComp_
# include <qobject.h>
#endif

#include "Workbench.h"
#include <MisGui/ToolBarManager.h>
#include <MisGui/MenuManager.h>
#include <MisGui/ToolBarManager.h>
#include <MisGui/DockWindowManager.h>
#include <MisGui/Application.h>
#include <MisGui/Action.h>
#include <MisGui/Command.h>
#include <MisGui/Selection.h>
#include <MisGui/ToolBoxManager.h>
#include <MisApp/Document.h>
#include <MisApp/DocumentObject.h>
#include <MisBase/Console.h>
#include <MisBase/Exception.h>

#include "StartConfiguration.h"

using namespace StartGui;

TYPESYSTEM_SOURCE(StartGui::Workbench, Gui::StdWorkbench)

StartGui::Workbench::Workbench()
{
}

StartGui::Workbench::~Workbench()
{
}

void StartGui::Workbench::activated()
{
    try {
        //Gui::Command::doCommand(Gui::Command::Gui,"import WebGui");
        //Gui::Command::doCommand(Gui::Command::Gui,"from StartPage import StartPage");
        //Gui::Command::doCommand(Gui::Command::Gui,"WebGui.openBrowserHTML"
        //"(StartPage.handle(),App.getResourceDir() + 'Mod/Start/StartPage/','Start page')");
    }
    catch (const Base::Exception& e) {
        Base::Console().Error("%s\n", e.what());
    }
}

void StartGui::Workbench::setupContextMenu(const char* recipient,Gui::MenuItem* item) const
{

}

Gui::MenuItem* StartGui::Workbench::setupMenuBar() const
{
    return Gui::StdWorkbench::setupMenuBar();
}

Gui::ToolBarItem* StartGui::Workbench::setupToolBars() const
{
    Gui::ToolBarItem* root = StdWorkbench::setupToolBars();

    // web navigation toolbar
    //Gui::ToolBarItem* navigation = new Gui::ToolBarItem(root);
    //navigation->setCommand("Navigation");
    //*navigation << "Web_OpenWebsite" 
    //            << "Separator" 
    //            << "Web_BrowserBack" 
    //            << "Web_BrowserNext" 
    //            << "Web_BrowserRefresh"
    //            << "Web_BrowserStop"
    //            << "Separator"
    //            << "Web_BrowserZoomIn"
    //            << "Web_BrowserZoomOut";

    return root;

}

Gui::ToolBarItem* StartGui::Workbench::setupCommandBars() const
{
    Gui::ToolBarItem* root = new Gui::ToolBarItem;
    return root;
}

Gui::DockWindowItems* StartGui::Workbench::setupDockWindows() const
{
    Gui::DockWindowItems* root = Gui::StdWorkbench::setupDockWindows();
    root->setVisibility(false); // hide all dock windows by default
    root->setVisibility("Std_CombiView",true); // except of the combi view
    return root;
}
