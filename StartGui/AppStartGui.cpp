#include "PreCompiled.h"
#ifndef _PreComp_
# include <Python.h>
#endif

#include <CXX/Extensions.hxx>
#include <CXX/Objects.hxx>

#include <MisBase/Console.h>
#include <MisBase/Interpreter.h>
#include <MisGui/Application.h>
#include <MisGui/WorkbenchManager.h>
#include <MisGui/Language/Translator.h>
#include "Workbench.h"

#include "StartConfiguration.h"


// use a different name to CreateCommand()
void CreateStartCommands(void);

void loadStartResource()
{
    // add resources and reloads the translators
    Q_INIT_RESOURCE(Start);
    Gui::Translator::instance()->refresh();
}

namespace StartGui {
class Module : public Py::ExtensionModule<Module>
{
public:
    Module() : Py::ExtensionModule<Module>("StartGui")
    {
        initialize("This module is the StartGui module."); // register with Python
    }

    virtual ~Module() {}

private:
};
} // namespace StartGui


/* Python entry */
PyMODINIT_FUNC initStartGui()
{
    if (!Gui::Application::Instance) {
        PyErr_SetString(PyExc_ImportError, "Cannot load Gui module in console application.");
        return;
    }

    // load dependent module
    try {
        //Base::Interpreter().runString("import WebGui");
    }
    catch(const Base::Exception& e) {
        PyErr_SetString(PyExc_ImportError, e.what());
        return;
    }
    catch (Py::Exception& e) {
        Py::Object o = Py::type(e);
        if (o.isString()) {
            Py::String s(o);
            Base::Console().Error("%s\n", s.as_std_string("utf-8").c_str());
        }
        else {
            Py::String s(o.repr());
            Base::Console().Error("%s\n", s.as_std_string("utf-8").c_str());
        }
        // Prints message to console window if we are in interactive mode
        PyErr_Print();
    }

    new StartGui::Module();
    Base::Console().Log("Loading GUI of Start module... done\n");

    // instantiating the commands
    CreateStartCommands();
    StartGui::Workbench::init();

     // add resources and reloads the translators
    loadStartResource();
}
